# node-csv

## Introduction
This project leverages the node-csv library for parsing and generating CSV files.

## Available APIs
1. **csv-generate**: a flexible generator for creating CSV strings and JavaScript objects.

2. **csv-parse**: a parser that converts CSV text into arrays or objects.

3. **csv-stringify**: a stringizer that transforms records into CSV text.

4. **stream-transform**: a simple object transformation framework that extends the native Node.js stream transformation API.

## How to Install


````
cd entry
ohpm install csv
````
For details about the OpenHarmony ohpm environment configuration, see [Installing the OpenHarmony HAR](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.en.md).

## How to Use

### csv-stringify
````javascript
import {stringify} from 'csv-stringify/browser/esm'
````
````javascript
let content = "1 2 3 4 5"
stringify(content, (err, output) => {
    // output -> CSV text
    let filePath = parentPath + '/temp.csv';
    console.log(TAG + "stringify filePath::" + filePath)
    let csvStr = '\ufeff' + output
    let openSync = fileio.openSync(filePath, 0o102, 0o666);
    try {
        // Use the file I/O function to generate a CSV file.
        let bytesWritten = fileio.writeSync(openSync, csvStr);
        this.text1 = "Generated value: " + JSON.stringify(csvStr);
    } catch (err) {
        console.log(TAG + 'stringify write error :' + err)
    }
});
````

### csv-parse
````javascript
import {parse} from 'csv-parse/browser/esm'
````
````javascript
parseToCsvString(filePath) {
    fileio.readText(filePath)
        .then((input) => {
            parse(input, {
                relax_column_count: true,
                skip_empty_lines: true
            }, (err, records) => {
                this.text2 = "Parsed value: " + JSON.stringify(records);
            });
        }).catch((e) => {
        console.log(TAG + 'parse read error :' + e)
    })
}
````

### csv-generate
````javascript
import {generate} from 'csv-generate/browser/esm'
````
````javascript
generateCsArray(){
    generate({
        seed: 1,
        objectMode: true,
        columns: 2,
        length: 2
    }, (err, records) => {
        this.text3 = "Generated CSV dataset: " + JSON.stringify(records);
    });
}
````

### csv-transform
````javascript
import {transform} from 'csv-generate/browser/esm'
````
````javascript
transformArry() {
    transform([['1', '2', '3', '4'], ['a', 'b', 'c', 'd']], (record) => {
        record.push(record.shift());
        return record;
    }, (err, output) => {
        this.text4 = "Transformed CSV dataset: " + JSON.stringify(output);
    });
}
````
> **NOTE**
>
> Currently, emojis are not supported for display or storage in CSV files in OpenHarmony.

## Directory Structure
````
/node-csv # Source code
|---- entry  # Framework code
|   |---- src   
|       |---- main  
|           |---- ets  
|               |---- MainAbility  # Demo code
|                   |---- pages
|                   |---- test                    
````
## Constraints

This project has been verified in the following versions:

DevEco Studio: 4.1 Canary (4.1.3.317), OpenHarmony SDK: API 11 (4.1.0.36)

## How to Contribute
If you find any problem when using the project, submit an [issue](https://gitee.com/openharmony-tpc/node-csv/issues) or a [PR](https://gitee.com/openharmony-tpc/node-csv/pulls).

## License
This project is licensed under [MIT License](./LICENSE).
