# node-csv

## 简介
该项目使用node-csv解析csv文件，生成csv文件。

## 功能说明
1.csv-generate， CSV 字符串和 Javascript 对象的灵活生成器。

2.csv-parse，将 CSV 文本转换为数组或对象的解析器。

3.csv-stringify，将记录转换为 CSV 文本的字符串化器。

4.stream-transform，简单对象转换框架，扩展原生 Node.js转换流 API。

## 下载安装
1.安装命令如下:

````
cd entry
ohpm install csv
````
对于OpenHarmony ohpm环境配置的详细信息，请参阅[OpenHarmony环境配置指南](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.md)。

## 使用说明

### csv-stringify
````javascript
import {stringify} from 'csv-stringify/browser/esm'
````
````javascript
let content = "1 2 3 4 5"
stringify(content, (err, output) => {
    //output ->csv格式的字符串
    let filePath = parentPath + '/temp.csv';
    console.log(TAG + "stringify filePath::" + filePath)
    let csvStr = '\ufeff' + output
    let openSync = fileio.openSync(filePath, 0o102, 0o666);
    try {
        //使用os文件读写功能生成csv文件
        let bytesWritten = fileio.writeSync(openSync, csvStr);
        this.text1 = "生成值： " + JSON.stringify(csvStr);
    } catch (err) {
        console.log(TAG + 'stringify write error :' + err)
    }
});
````

### csv-parse
````javascript
import {parse} from 'csv-parse/browser/esm'
````
````javascript
parseToCsvString(filePath) {
    fileio.readText(filePath)
        .then((input) => {
            parse(input, {
                relax_column_count: true,
                skip_empty_lines: true
            }, (err, records) => {
                this.text2 = "解析值： " + JSON.stringify(records);
            });
        }).catch((e) => {
        console.log(TAG + 'parse read error :' + e)
    })
}
````

### csv-generate
````javascript
import {generate} from 'csv-generate/browser/esm'
````
````javascript
generateCsArray(){
    generate({
        seed: 1,
        objectMode: true,
        columns: 2,
        length: 2
    }, (err, records) => {
        this.text3 = "生成CSV数据集： " + JSON.stringify(records);
    });
}
````

### csv-transform
````javascript
import {transform} from 'csv-generate/browser/esm'
````
````javascript
transformArry() {
    transform([['1', '2', '3', '4'], ['a', 'b', 'c', 'd']], (record) => {
        record.push(record.shift());
        return record;
    }, (err, output) => {
        this.text4 = "转变CSV数据集： " + JSON.stringify(output);
    });
}
````
注意:
由于OpenHarmony当前不支持emoji表情显示，所以当前并不支持emoji表情存储到csv中。

## 项目目录
````
/node-csv #三方库源代码
|---- entry  #框架代码
|   |---- src   
|       |---- main  
|           |---- ets  
|               |---- MainAbility  # demo代码
|                   |---- pages
|                   |---- test                    
````
## 约束与限制

在下述版本验证通过：

DevEco Studio 版本： 4.1 Canary(4.1.3.317),OpenHarmony SDK:API11 (4.1.0.36)

## 参与贡献
使用过程中发现任何问题都可以提交 [Issue](https://gitee.com/openharmony-tpc/node-csv/issues)，当然，也非常欢迎提交 [PR](https://gitee.com/openharmony-tpc/node-csv/pulls) 。

## 开源协议
本项目遵循 [MIT License](./LICENSE)。

